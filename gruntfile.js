"use strict";

module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON("package.json"),
		compass: {
			compile: {
				options: {
					cssDir: "css",
					outputStyle: "compressed",
					sassDir: "resources/sass",
					sourcemap: true
				}
			}
		},
		copy: {
			main: {
				files: [
					{
						expand: true,
						cwd: "node_modules/",
						src: [
							"semantic-ui-css/semantic.min.css"
						],
						dest: "css/vendor",
						flatten: true,
						filter: "isFile"
					},
					{
						expand: true,
						cwd: "node_modules/",
						src: [
							"jquery/dist/jquery.min.*",
							"semantic-ui-css/semantic.min.js",
							"vue/dist/vue.min*",
							"vue-resource/dist/vue-resource.min.js"
						],
						dest: "js/vendor",
						flatten: true,
						filter: "isFile"
					},
					{
						expand: true,
						cwd: "node_modules/",
						src: [
							"semantic-ui-css/themes/default/assets/fonts/*",
						],
						dest: "css/vendor/themes/default/assets/fonts/",
						flatten: true,
						filter: "isFile"
					},
					{
						expand: true,
						cwd: "node_modules/",
						src: [
							"semantic-ui-css/themes/default/assets/images/*",
						],
						dest: "css/vendor/themes/default/assets/images/",
						flatten: true,
						filter: "isFile"
					},
				]
			}
		},
		uglify: {
			files: { 
				src: "resources/js/*.js",
				dest: "js/",
				expand: true,
				flatten: true,
				ext: ".js"
			}
		},
		watch: {
			css:  { 
				files: "resources/sass/*.scss", 
				tasks: [
					"compass"
				]
			},
			scripts:  { 
				files: "resources/js/*.js", 
				tasks: [
					"uglify"
				]
			},
		}
	});

	grunt.loadNpmTasks("grunt-contrib-compass");
	grunt.loadNpmTasks("grunt-contrib-copy");
	grunt.loadNpmTasks("grunt-contrib-uglify");
	grunt.loadNpmTasks("grunt-contrib-watch");

	grunt.registerTask("default", ["compass", "copy", "uglify"]);
};
