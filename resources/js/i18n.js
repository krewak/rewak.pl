var i18n = new Vue({
	el: "#i18n",
	data: {
		translated: false,
	},
	created: function() {
		if(window.navigator.language != "pl") {
			this.translated = true;
		}
	},
	methods: {
		toggleLanguage: function() {
			this.translated = !this.translated;
		},
	}
});
